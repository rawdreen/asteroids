#include "MMenu.h"
#include <SFML\Graphics.hpp>
#include <fstream>

using namespace sf;





void Options(int H, int W)
{
	Font font;

	font.loadFromFile("font/BAUHS93.ttf");
	Text text;

	Text caption;

	RenderWindow app(VideoMode(W, H), "Options");

	Texture t1;

	t1.loadFromFile("images/background.png");

	Sprite bg(t1);

	while (app.isOpen())
	{
		Event e;
		while (app.pollEvent(e))
		{
			if (e.type == Event::Closed)
			{
				app.close();
			}


		}
		text.setFont(font);
		text.setString("Score: ");
		text.setCharacterSize(24);
		text.setColor(Color::White);
		text.setPosition(0, 0);
		app.draw(text);

		app.draw(bg);
		app.display();
	}
}