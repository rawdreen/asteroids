#include "Menu.h"


Menu::Menu(float width, float height)
{
	if (!font.loadFromFile("font/BAUHS93.ttf"))
	{ /* handle error*/}

	menu[0].setOrigin(30, 0);
	menu[0].setFont(font);
	menu[0].setColor(Color::Magenta);
	menu[0].setString("Play");
	menu[0].setPosition(Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 1));

	menu[1].setOrigin(30, 0);
	menu[1].setFont(font);
	menu[1].setColor(Color::Magenta);
	menu[1].setString("Options");
	menu[1].setPosition(Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 2));


	menu[2].setOrigin(30, 0);
	menu[2].setFont(font);
	menu[2].setColor(Color::Magenta);
	menu[2].setString("Exit");
	menu[2].setPosition(Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 3));

	selectedItemIndex = 0;
}


Menu::~Menu()
{
}

void Menu::draw(RenderWindow &app)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		app.draw(menu[i]);
	}
}


void Menu::MoveUp()
{
	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setColor(Color::Magenta);
		selectedItemIndex--;
		menu[selectedItemIndex].setColor(Color::Black);
	}
}

void Menu::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[selectedItemIndex].setColor(Color::Magenta);
		selectedItemIndex++;
		menu[selectedItemIndex].setColor(Color::Black);
	}
}


/////////////FOR BUTTON VERSION///////////////