#include "SFML\Graphics.hpp";
#include <iostream>
#include "Menu.h"
#include "MMenu.h"
#include "Doodle_Jump.h"

using namespace sf;
using namespace std;

const int W = 400;
const int H = 533;

void menu()
{
	RenderWindow app(VideoMode(W, H), "Menu", Style::Close | Style::Close);

	Texture t1;
	t1.loadFromFile("images/background.png");

	Sprite background(t1);

	Menu menu(app.getSize().x, app.getSize().y);

	while (app.isOpen())
	{
		Event e;
		while (app.pollEvent(e))
		{

			switch (e.type)
			{
			case Event::KeyReleased:
				switch (e.key.code)
				{
				case Keyboard::Up:
					menu.MoveUp();
					break;

				case Keyboard::Down:
					menu.MoveDown();
					break;

				case Keyboard::Return:
					switch (menu.GetPressedItem())
					{
					case 0:
						cout << "Play button has been pressed\n";
						app.close();
						Game();
						break;
					case 1:
						cout << "Settings button has been pressed\n";
						app.close();
						Options(H,W);
						break;
					case 2:
						cout << "Exit button has been pressed\n";
						app.close();
						break;
					}

					break;
				}

				break;

			case Event::Closed:
				app.close();
			}
		}

		app.clear();
		app.draw(background);
		menu.draw(app);
		app.display();

	}


}


/////////////FOR BUTTON VERSION///////////////